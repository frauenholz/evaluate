/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    Collection,
    ConditionBlock,
    DateTime,
    Forms,
    L10n,
    NodeBlock,
    Slots,
    Str,
    affects,
    definition,
    each,
    editor,
    insertVariable,
    isFilledString,
    isNumberFinite,
    isString,
    isVariable,
    lookupVariable,
    makeMarkdownSafe,
    map,
    markdownifyToString,
    pgettext,
    populateVariables,
    tripetto,
} from "tripetto";
import { IEvaluateCondition } from "../runner";
import { TMode } from "../runner/mode";

/** Assets */
import ICON from "../../assets/icon.svg";
import { TTextSuggestions } from "tripetto/forms";

@tripetto({
    type: "condition",
    context: "*",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    autoOpen: true,
    get label() {
        return pgettext("block:evaluate", "Evaluate");
    },
})
export class EvaluateCondition
    extends ConditionBlock
    implements IEvaluateCondition
{
    readonly allowMarkdown = true;

    @definition
    @affects("#name")
    variable?: string;

    @definition
    @affects("#name")
    mode?: TMode;

    @definition
    @affects("#name")
    value?: string | number;

    @definition
    @affects("#name")
    to?: string | number;

    @definition
    ignoreCase?: boolean;

    private get isString() {
        return (
            this.slot instanceof Slots.String ||
            this.slot instanceof Slots.Text ||
            (!this.slot && this.variable ? true : false)
        );
    }

    private get isNumber() {
        return (
            this.slot instanceof Slots.Number ||
            this.slot instanceof Slots.Numeric
        );
    }

    private get isDate() {
        return this.slot instanceof Slots.Date;
    }

    private get supportsTime() {
        return this.slot instanceof Slots.Date && this.slot.supportsTime;
    }

    private get isBoolean() {
        return this.slot instanceof Slots.Boolean;
    }

    // Return an empty label, since the variable name is in the block name already.
    get label() {
        return "";
    }

    get name(): string {
        if (this.variable && lookupVariable(this, this.variable)) {
            if (this.mode === "defined") {
                return `@${this.variable} ${pgettext(
                    "block:evaluate",
                    "not empty"
                )}`;
            }

            if (this.mode === "undefined") {
                return `@${this.variable} ${pgettext(
                    "block:evaluate",
                    "empty"
                )}`;
            }

            if (this.isString) {
                const wrap = (s: string) =>
                    (s &&
                        (this.mode === "contains" ||
                            this.mode === "not-contains" ||
                            this.mode === "starts" ||
                            this.mode === "ends") &&
                        `_${s}_`) ||
                    s;
                const match =
                    (isString(this.value) && isVariable(this.value)
                        ? lookupVariable(this, this.value)?.label &&
                          `@${this.value}`
                        : isFilledString(this.value) &&
                          wrap(
                              makeMarkdownSafe(
                                  Str.replace(this.value, "\n", "↵")
                              )
                          )) || "\\_\\_";

                switch (this.mode) {
                    case "equal":
                    case "not-equal":
                        return `@${this.variable} ${
                            this.mode === "not-equal" ? "\u2260" : "="
                        } ${match}`;
                    case "contains":
                    case "not-contains":
                    case "starts":
                    case "ends":
                        return `@${this.variable} ${
                            this.mode === "not-contains"
                                ? pgettext("block:evaluate", "does not contain")
                                : this.mode === "starts"
                                ? pgettext("block:evaluate", "starts with")
                                : this.mode === "ends"
                                ? pgettext("block:evaluate", "ends with")
                                : pgettext("block:evaluate", "contains")
                        } ${match}`;
                }
            }

            if (this.isNumber) {
                const slot = this.slot as Slots.Number | Slots.Numeric;
                const parse = (
                    s: Slots.Number | Slots.Numeric,
                    v: number | string | undefined
                ) => {
                    if (isNumberFinite(v)) {
                        return s instanceof Slots.Numeric
                            ? s.toString(v, (n, p) =>
                                  L10n.locale.number(n, p, false)
                              )
                            : s.toString(v, (n) =>
                                  L10n.locale.number(n, 0, false)
                              );
                    } else if (
                        isString(v) &&
                        v &&
                        lookupVariable(this, v)?.label
                    ) {
                        return "@" + v;
                    }

                    return "\\_\\_";
                };
                const value = parse(slot, this.value);

                switch (this.mode) {
                    case "between":
                        return `${value} ≤ @${this.variable} ≤ ${parse(
                            slot,
                            this.to
                        )}`;
                    case "not-between":
                        return `@${this.variable} < ${value} ${pgettext(
                            "block:evaluate",
                            "or"
                        )} @${this.variable} > ${parse(slot, this.to)}`;
                    case "not-equal":
                        return `@${this.variable} \u2260 ${value}`;
                    case "above":
                    case "below":
                    case "equal":
                        return `@${this.variable} ${
                            this.mode === "above"
                                ? ">"
                                : this.mode === "below"
                                ? "<"
                                : "="
                        } ${value}`;
                }
            }

            if (this.isDate) {
                const slot = this.slot as Slots.Date;
                const parse = (
                    s: Slots.Date,
                    v: number | string | undefined
                ) => {
                    if (isNumberFinite(v)) {
                        return s.supportsTime
                            ? L10n.locale.dateTimeShort(s.toValue(v), true)
                            : L10n.locale.dateShort(s.toValue(v), true);
                    } else if (isString(v)) {
                        if (v && lookupVariable(this, v)?.label) {
                            return `@${v}`;
                        }

                        return "\\_\\_";
                    }

                    return (
                        "`" +
                        pgettext("block:evaluate", "Now").toUpperCase() +
                        "`"
                    );
                };
                const value = parse(slot, this.value);

                switch (this.mode) {
                    case "between":
                        return `${value} ≤ @${slot.id} ≤ ${parse(
                            slot,
                            this.to
                        )}`;
                    case "not-between":
                        return `@${slot.id} < ${value} ${pgettext(
                            "block:evaluate",
                            "or"
                        )} @${slot.id} > ${parse(slot, this.to)}`;
                    case "before":
                    case "after":
                    case "equal":
                    case "not-equal":
                        return `@${slot.id} ${
                            this.mode === "after"
                                ? ">"
                                : this.mode === "before"
                                ? "<"
                                : this.mode === "not-equal"
                                ? "\u2260"
                                : "="
                        } ${value}`;
                }
            }

            if (this.isBoolean) {
                const slot = this.slot as Slots.Boolean;

                switch (this.mode) {
                    case "true":
                        return `@${this.variable} = ${
                            makeMarkdownSafe(slot.labelForTrue || "") ||
                            pgettext("block:evaluate", "True")
                        }`;
                    case "false":
                        return `@${this.variable} = ${
                            makeMarkdownSafe(slot.labelForFalse || "") ||
                            pgettext("block:evaluate", "False")
                        }`;
                    case "equal":
                    case "not-equal":
                        return `@${this.variable} ${
                            this.mode === "not-equal" ? "\u2260" : "="
                        } ${
                            isString(this.value) &&
                            isVariable(this.value) &&
                            lookupVariable(this, this.value)?.label
                                ? `@${this.value}`
                                : "\\_\\_"
                        }`;
                }
            }
        }

        return this.type.label;
    }

    private static getToday(s: "begin" | "end"): number {
        return DateTime.UTCToday + (s === "end" ? 86400000 - 1 : 0);
    }

    private stringEditor(): {
        readonly update: (
            oldVariable: string | undefined,
            newVariable: string | undefined
        ) => void;
    } {
        const ref = this.editor
            .form({
                title: pgettext("block:evaluate", "Compare mode"),
                controls: [
                    new Forms.Radiobutton<TMode>(
                        [
                            {
                                label: pgettext(
                                    "block:evaluate",
                                    "Value matches"
                                ),
                                value: "equal",
                            },
                            {
                                label: pgettext(
                                    "block:evaluate",
                                    "Value does not match"
                                ),
                                value: "not-equal",
                            },
                            {
                                label: pgettext(
                                    "block:evaluate",
                                    "Value contains"
                                ),
                                value: "contains",
                            },
                            {
                                label: pgettext(
                                    "block:evaluate",
                                    "Value does not contain"
                                ),
                                value: "not-contains",
                            },
                            {
                                label: pgettext(
                                    "block:evaluate",
                                    "Value starts with"
                                ),
                                value: "starts",
                            },
                            {
                                label: pgettext(
                                    "block:evaluate",
                                    "Value ends with"
                                ),
                                value: "ends",
                            },
                            {
                                label: pgettext(
                                    "block:evaluate",
                                    "Value is not empty"
                                ),
                                value: "defined",
                            },
                            {
                                label: pgettext(
                                    "block:evaluate",
                                    "Value is empty"
                                ),
                                value: "undefined",
                            },
                        ],
                        this.mode || "equal"
                    ).on((mode: Forms.Radiobutton<TMode>) => {
                        if (this.isString && mode.isObservable) {
                            this.mode = mode.value;
                        }

                        form.visible(
                            this.isString &&
                                mode.value !== "defined" &&
                                mode.value !== "undefined"
                        );

                        switch (mode.value) {
                            case "equal":
                                form.title = pgettext(
                                    "block:evaluate",
                                    "If value matches"
                                );
                                break;
                            case "not-equal":
                                form.title = pgettext(
                                    "block:evaluate",
                                    "If value does not match"
                                );
                                break;
                            case "contains":
                                form.title = pgettext(
                                    "block:evaluate",
                                    "If value contains"
                                );
                                break;
                            case "not-contains":
                                form.title = pgettext(
                                    "block:evaluate",
                                    "If value does not contain"
                                );
                                break;
                            case "starts":
                                form.title = pgettext(
                                    "block:evaluate",
                                    "If value starts with"
                                );
                                break;
                            case "ends":
                                form.title = pgettext(
                                    "block:evaluate",
                                    "If value ends with"
                                );
                                break;
                        }

                        if (
                            singlelineControl.isObservable &&
                            singlelineControl.isInteractable
                        ) {
                            singlelineControl.focus();
                        }

                        if (
                            multilineControl.isObservable &&
                            multilineControl.isInteractable
                        ) {
                            multilineControl.focus();
                        }
                    }),
                ],
            })
            .visible(this.isString);

        let soleSupply = false;
        const isVar = (isString(this.value) && isVariable(this.value)) || false;
        const variables = populateVariables(
            this,
            undefined,
            isVar ? (this.value as string) : undefined
        );
        const singlelineControl = new Forms.Text(
            "singleline",
            !isVar && isString(this.value) ? this.value : ""
        )
            .label(pgettext("block:evaluate", "Use fixed text"))
            .action("@", insertVariable(this))
            .autoFocus()
            .enter(this.editor.close)
            .escape(this.editor.close)
            .on((input) => {
                if (input.isFormVisible && input.isVisible) {
                    this.value = multilineControl.value = input.value;
                }

                ignoreCaseControl.disabled(
                    input.isFormVisible &&
                        input.isVisible &&
                        input.suggestion &&
                        soleSupply
                        ? true
                        : false
                );
            });
        const multilineControl = new Forms.Text(
            "multiline",
            !isVar && isString(this.value) ? this.value : ""
        )
            .label(pgettext("block:evaluate", "Use fixed text"))
            .action("@", insertVariable(this))
            .autoFocus()
            .enter(this.editor.close)
            .escape(this.editor.close)
            .on((input) => {
                if (input.isFormVisible && input.isVisible) {
                    this.value = singlelineControl.value = input.value;
                }
            });
        const variableControl = new Forms.Dropdown(
            variables,
            isVar ? (this.value as string) : ""
        )
            .label(pgettext("block:evaluate", "Use value of"))
            .width("full")
            .on((variable) => {
                if (
                    variable.isFormVisible &&
                    variable.isObservable &&
                    this.variable !== variable.value
                ) {
                    this.value = variable.value || undefined;
                }
            });

        const typeControl = new Forms.Radiobutton<"text" | "variable">(
            [
                {
                    label: pgettext("block:evaluate", "Text"),
                    value: "text",
                },
                {
                    label: pgettext("block:evaluate", "Value"),
                    value: "variable",
                    disabled: variables.length === 0,
                },
            ],
            isVar ? "variable" : "text"
        ).on((type) => {
            singlelineControl.visible(
                (!singlelineControl.isDisabled && type.value === "text") ||
                    false
            );
            multilineControl.visible(
                (!multilineControl.isDisabled && type.value === "text") || false
            );
            variableControl.visible(type.value === "variable");

            if (
                singlelineControl.isObservable &&
                singlelineControl.isInteractable
            ) {
                singlelineControl.focus();
            }

            if (
                multilineControl.isObservable &&
                multilineControl.isInteractable
            ) {
                multilineControl.focus();
            }
        });

        const ignoreCaseControl = new Forms.Checkbox(
            pgettext("block:evaluate", "Ignore case"),
            Forms.Checkbox.bind(this, "ignoreCase", undefined, true)
        );

        const updateSuggestions = () => {
            const variable = this.slot && lookupVariable(this, this.slot.id);
            let suggestions: TTextSuggestions | undefined;

            soleSupply = false;

            if (
                variable &&
                variable.slot &&
                variable.block instanceof NodeBlock
            ) {
                const supply = Collection.find(variable);

                if (supply) {
                    soleSupply = supply.sole;
                    suggestions = map(supply.collection.all, (item) =>
                        markdownifyToString(item.getNameOfItem())
                    );
                }
            }

            singlelineControl.suggestions(suggestions);
            singlelineControl.disabled(suggestions ? false : true);
            multilineControl.disabled(suggestions ? true : false);
            typeControl.refresh();
        };

        updateSuggestions();

        const form = this.editor
            .form({
                title: pgettext("block:evaluate", "If value matches"),
                controls: [
                    typeControl,
                    singlelineControl,
                    multilineControl,
                    variableControl,
                    ignoreCaseControl,
                ],
            })
            .visible(
                this.isString &&
                    this.mode !== "defined" &&
                    this.mode !== "undefined"
            );

        return {
            update: (
                oldVariable: string | undefined,
                newVariable: string | undefined
            ) => {
                ref.visible(this.isString);

                if (oldVariable !== newVariable) {
                    if (oldVariable) {
                        variableControl.optionDisabled(oldVariable, false);
                    }

                    if (newVariable) {
                        variableControl.optionDisabled(newVariable, true);
                    }
                }

                updateSuggestions();
            },
        };
    }

    private numberEditor(): {
        readonly update: (
            oldVariable: string | undefined,
            newVariable: string | undefined
        ) => void;
    } {
        const variableControls: Forms.Dropdown<string>[] = [];
        const ref = this.editor
            .form({
                title: pgettext("block:evaluate", "Compare mode"),
                controls: [
                    new Forms.Radiobutton<TMode>(
                        [
                            {
                                label: pgettext(
                                    "block:evaluate",
                                    "Number is equal to"
                                ),
                                value: "equal",
                            },
                            {
                                label: pgettext(
                                    "block:evaluate",
                                    "Number is not equal to"
                                ),
                                value: "not-equal",
                            },
                            {
                                label: pgettext(
                                    "block:evaluate",
                                    "Number is lower than"
                                ),
                                value: "below",
                            },
                            {
                                label: pgettext(
                                    "block:evaluate",
                                    "Number is higher than"
                                ),
                                value: "above",
                            },
                            {
                                label: pgettext(
                                    "block:evaluate",
                                    "Number is between"
                                ),
                                value: "between",
                            },
                            {
                                label: pgettext(
                                    "block:evaluate",
                                    "Number is not between"
                                ),
                                value: "not-between",
                            },
                            {
                                label: pgettext(
                                    "block:evaluate",
                                    "Number is not empty"
                                ),
                                value: "defined",
                            },
                            {
                                label: pgettext(
                                    "block:evaluate",
                                    "Number is empty"
                                ),
                                value: "undefined",
                            },
                        ],
                        this.mode || "equal"
                    ).on((mode: Forms.Radiobutton<TMode>) => {
                        if (this.isNumber && mode.isObservable) {
                            this.mode = mode.value;
                        }

                        from.visible(
                            this.isNumber &&
                                mode.value !== "defined" &&
                                mode.value !== "undefined"
                        );
                        to.visible(
                            this.isNumber &&
                                (mode.value === "between" ||
                                    mode.value === "not-between")
                        );

                        switch (mode.value) {
                            case "equal":
                                from.title = pgettext(
                                    "block:evaluate",
                                    "If number equals"
                                );
                                break;
                            case "not-equal":
                                from.title = pgettext(
                                    "block:evaluate",
                                    "If number not equals"
                                );
                                break;
                            case "below":
                                from.title = pgettext(
                                    "block:evaluate",
                                    "If number is lower than"
                                );
                                break;
                            case "above":
                                from.title = pgettext(
                                    "block:evaluate",
                                    "If number is higher than"
                                );
                                break;
                            case "between":
                                from.title = pgettext(
                                    "block:evaluate",
                                    "If number is between"
                                );
                                break;
                            case "not-between":
                                from.title = pgettext(
                                    "block:evaluate",
                                    "If number is not between"
                                );
                                break;
                        }
                    }),
                ],
            })
            .visible(this.isNumber);

        const addCondition = (
            property: "value" | "to",
            title: string,
            visible: boolean
        ): [Forms.Form, Forms.Numeric] => {
            const value = this[property];
            const src =
                this.slot instanceof Slots.Numeric ? this.slot : undefined;
            const numberControl = new Forms.Numeric(
                isNumberFinite(value) ? value : 0
            )
                .label(pgettext("block:evaluate", "Use fixed number"))
                .precision(src?.precision || 0)
                .digits(src?.digits || 0)
                .decimalSign(src?.decimal || "")
                .thousands(src?.separator ? true : false, src?.separator || "")
                .prefix(src?.prefix || "")
                .prefixPlural(src?.prefixPlural || undefined)
                .suffix(src?.suffix || "")
                .suffixPlural(src?.suffixPlural || undefined)
                .min(src?.minimum)
                .max(src?.maximum)
                .autoFocus(property === "value")
                .escape(this.editor.close)
                .enter(
                    () =>
                        ((this.mode !== "between" &&
                            this.mode !== "not-between") ||
                            property === "to") &&
                        this.editor.close()
                )
                .on((input) => {
                    if (input.isFormVisible && input.isObservable) {
                        this[property] = input.value;
                    }
                });

            const isVar = (isString(value) && isVariable(value)) || false;
            const variables = populateVariables(
                this,
                (slot) =>
                    slot instanceof Slots.Number ||
                    slot instanceof Slots.Numeric,
                isVar ? (value as string) : undefined
            );
            const variableControl = new Forms.Dropdown(
                variables,
                isVar ? (value as string) : ""
            )
                .label(pgettext("block:evaluate", "Use value of"))
                .width("full")
                .on((variable) => {
                    if (
                        variable.isFormVisible &&
                        variable.isObservable &&
                        this.variable !== variable.value
                    ) {
                        this[property] = variable.value || "";
                    }
                });

            variableControls.push(variableControl);

            return [
                this.editor
                    .form({
                        title,
                        controls: [
                            new Forms.Radiobutton<"number" | "variable">(
                                [
                                    {
                                        label: pgettext(
                                            "block:evaluate",
                                            "Number"
                                        ),
                                        value: "number",
                                    },
                                    {
                                        label: pgettext(
                                            "block:evaluate",
                                            "Value"
                                        ),
                                        value: "variable",
                                        disabled: variables.length === 0,
                                    },
                                ],
                                isVar ? "variable" : "number"
                            ).on((type) => {
                                numberControl.visible(type.value === "number");
                                variableControl.visible(
                                    type.value === "variable"
                                );

                                if (numberControl.isObservable) {
                                    numberControl.focus();
                                }
                            }),
                            numberControl,
                            variableControl,
                        ],
                    })
                    .visible(visible),
                numberControl,
            ];
        };

        const [from, fromNumber] = addCondition(
            "value",
            pgettext("block:evaluate", "If number equals"),
            this.isNumber &&
                this.mode !== "defined" &&
                this.mode !== "undefined"
        );
        const [to, toNumber] = addCondition(
            "to",
            pgettext("block:evaluate", "And"),
            this.isNumber &&
                (this.mode === "between" || this.mode === "not-between")
        );

        return {
            update: (
                oldVariable: string | undefined,
                newVariable: string | undefined
            ) => {
                const fnUpdate = (numberControl: Forms.Numeric) => {
                    numberControl.precision(
                        (this.slot instanceof Slots.Numeric &&
                            this.slot.precision) ||
                            0
                    );
                    numberControl.digits(
                        (this.slot instanceof Slots.Numeric &&
                            this.slot.digits) ||
                            0
                    );
                    numberControl.decimalSign(
                        (this.slot instanceof Slots.Numeric &&
                            this.slot.decimal) ||
                            ""
                    );
                    numberControl.thousands(
                        this.slot instanceof Slots.Numeric &&
                            this.slot.separator
                            ? true
                            : false,
                        (this.slot instanceof Slots.Numeric &&
                            this.slot.separator) ||
                            ""
                    );
                    numberControl.min(
                        (this.slot instanceof Slots.Numeric &&
                            this.slot.minimum) ||
                            undefined
                    );
                    numberControl.max(
                        (this.slot instanceof Slots.Numeric &&
                            this.slot.maximum) ||
                            undefined
                    );
                    numberControl.prefix(
                        (this.slot instanceof Slots.Numeric &&
                            this.slot.prefix) ||
                            ""
                    );
                    numberControl.prefixPlural(
                        (this.slot instanceof Slots.Numeric &&
                            this.slot.prefixPlural) ||
                            undefined
                    );
                    numberControl.suffix(
                        (this.slot instanceof Slots.Numeric &&
                            this.slot.suffix) ||
                            ""
                    );
                    numberControl.suffixPlural(
                        (this.slot instanceof Slots.Numeric &&
                            this.slot.suffixPlural) ||
                            undefined
                    );
                };

                fnUpdate(fromNumber);
                fnUpdate(toNumber);

                ref.visible(this.isNumber);

                if (oldVariable !== newVariable) {
                    if (oldVariable) {
                        each(variableControls, (variableControl) => {
                            variableControl.optionDisabled(oldVariable, false);
                        });
                    }

                    if (newVariable) {
                        each(variableControls, (variableControl) => {
                            variableControl.optionDisabled(newVariable, true);
                        });
                    }
                }
            },
        };
    }

    private booleanEditor(): {
        readonly update: (
            oldVariable: string | undefined,
            newVariable: string | undefined
        ) => void;
    } {
        const booleanMode = new Forms.Radiobutton<TMode>(
            [
                {
                    label:
                        (this.slot instanceof Slots.Boolean &&
                            this.slot.labelForTrue) ||
                        pgettext("block:evaluate", "Is true"),
                    value: "true",
                },
                {
                    label:
                        (this.slot instanceof Slots.Boolean &&
                            this.slot.labelForFalse) ||
                        pgettext("block:evaluate", "Is false"),
                    value: "false",
                },
                {
                    label: pgettext(
                        "block:evaluate",
                        "Equals another variable"
                    ),
                    value: "equal",
                },
                {
                    label: pgettext(
                        "block:evaluate",
                        "Not equals another variable"
                    ),
                    value: "not-equal",
                },
                {
                    label: pgettext("block:evaluate", "Is not empty"),
                    value: "defined",
                },
                {
                    label: pgettext("block:evaluate", "Is empty"),
                    value: "undefined",
                },
            ],
            this.mode || "true"
        ).on((mode: Forms.Radiobutton<TMode>) => {
            if (this.isBoolean && mode.isObservable) {
                this.mode = mode.value;
            }

            if (
                this.isBoolean &&
                mode.value !== "equal" &&
                mode.value !== "not-equal"
            ) {
                this.value = undefined;
            }

            form.visible(
                this.isBoolean &&
                    (mode.value === "equal" || mode.value === "not-equal")
            );

            switch (mode.value) {
                case "equal":
                    form.title = pgettext("block:evaluate", "If value matches");
                    break;
                case "not-equal":
                    form.title = pgettext(
                        "block:evaluate",
                        "If value does not match"
                    );
                    break;
            }
        });
        const ref = this.editor
            .form({
                title: pgettext("block:evaluate", "Compare mode"),
                controls: [booleanMode],
            })
            .visible(this.isBoolean);

        const isVar = (isString(this.value) && isVariable(this.value)) || false;
        const variables = populateVariables(
            this,
            (slot, pipe) => !pipe && slot instanceof Slots.Boolean,
            isVar ? (this.value as string) : undefined
        );
        const variableControl = new Forms.Dropdown(
            variables,
            isVar ? (this.value as string) : ""
        )
            .width("full")
            .on((variable) => {
                if (
                    variable.isFormVisible &&
                    variable.isObservable &&
                    this.variable !== variable.value
                ) {
                    this.value = variable.value || undefined;
                }
            });

        const form = this.editor
            .form({
                title: pgettext("block:evaluate", "If value matches"),
                controls: [variableControl],
            })
            .visible(
                this.isBoolean &&
                    (this.mode === "equal" || this.mode === "not-equal")
            );

        return {
            update: (
                oldVariable: string | undefined,
                newVariable: string | undefined
            ) => {
                ref.visible(this.isBoolean);

                if (oldVariable !== newVariable) {
                    if (oldVariable) {
                        variableControl.optionDisabled(oldVariable, false);
                    }

                    if (newVariable) {
                        variableControl.optionDisabled(newVariable, true);
                    }
                }

                booleanMode.buttonLabel(
                    "true",
                    (this.slot instanceof Slots.Boolean &&
                        this.slot.labelForTrue) ||
                        pgettext("block:evaluate", "Is true")
                );
                booleanMode.buttonLabel(
                    "false",
                    (this.slot instanceof Slots.Boolean &&
                        this.slot.labelForFalse) ||
                        pgettext("block:evaluate", "Is false")
                );
            },
        };
    }

    private dateEditor(): {
        readonly update: (
            oldVariable: string | undefined,
            newVariable: string | undefined
        ) => void;
    } {
        const variableControls: Forms.Dropdown<string>[] = [];
        const modeControl = new Forms.Radiobutton<TMode>(
            [
                {
                    label: pgettext("block:evaluate", "Date is equal to"),
                    value: "equal",
                },
                {
                    label: pgettext("block:evaluate", "Date is not equal to"),
                    value: "not-equal",
                },
                {
                    label: pgettext("block:evaluate", "Date is before"),
                    value: "before",
                },
                {
                    label: pgettext("block:evaluate", "Date is after"),
                    value: "after",
                },
                {
                    label: pgettext("block:evaluate", "Date is between"),
                    value: "between",
                },
                {
                    label: pgettext("block:evaluate", "Date is not between"),
                    value: "not-between",
                },
                {
                    label: pgettext("block:evaluate", "Date is not empty"),
                    value: "defined",
                },
                {
                    label: pgettext("block:evaluate", "Date is empty"),
                    value: "undefined",
                },
            ],
            this.mode || "equal"
        ).on((mode: Forms.Radiobutton<TMode>) => {
            if (this.isDate && mode.isObservable) {
                this.mode = mode.value;
            }

            fromDate.visible(
                this.isDate &&
                    !this.supportsTime &&
                    mode.value !== "defined" &&
                    mode.value !== "undefined"
            );
            fromDateTime.visible(
                this.isDate &&
                    this.supportsTime &&
                    mode.value !== "defined" &&
                    mode.value !== "undefined"
            );
            toDate.visible(
                this.isDate &&
                    !this.supportsTime &&
                    (mode.value === "between" || mode.value === "not-between")
            );
            toDateTime.visible(
                this.isDate &&
                    this.supportsTime &&
                    (mode.value === "between" || mode.value === "not-between")
            );

            switch (mode.value) {
                case "equal":
                    fromDate.title = pgettext(
                        "block:evaluate",
                        "If date equals"
                    );
                    fromDateTime.title = pgettext(
                        "block:evaluate",
                        "If date/time equals"
                    );
                    break;
                case "not-equal":
                    fromDate.title = pgettext(
                        "block:evaluate",
                        "If date not equals"
                    );
                    fromDateTime.title = pgettext(
                        "block:evaluate",
                        "If date/time not equals"
                    );
                    break;
                case "before":
                    fromDate.title = pgettext(
                        "block:evaluate",
                        "If date is before"
                    );
                    fromDateTime.title = pgettext(
                        "block:evaluate",
                        "If date/time is before"
                    );
                    break;
                case "after":
                    fromDate.title = pgettext(
                        "block:evaluate",
                        "If date is after"
                    );
                    fromDateTime.title = pgettext(
                        "block:evaluate",
                        "If date/time equals"
                    );
                    break;
                case "between":
                    fromDate.title = pgettext(
                        "block:evaluate",
                        "If date is between"
                    );
                    fromDateTime.title = pgettext(
                        "block:evaluate",
                        "If date/time is between"
                    );
                    break;
                case "not-between":
                    fromDate.title = pgettext(
                        "block:evaluate",
                        "If date is not between"
                    );
                    fromDateTime.title = pgettext(
                        "block:evaluate",
                        "If date/time is not between"
                    );
                    break;
            }
        });

        const form = this.editor
            .form({
                title: pgettext("block:evaluate", "Compare mode"),
                controls: [modeControl],
            })
            .visible(this.isDate);

        const addCondition = (
            property: "value" | "to",
            title: string,
            supportsTime: boolean,
            visible: boolean
        ) => {
            const value = this[property];
            const isVar = (isString(value) && isVariable(value)) || false;
            const variables = populateVariables(
                this,
                (slot) => slot instanceof Slots.Date,
                isVar ? (value as string) : undefined
            );
            const dateControl = new Forms.DateTime(
                isNumberFinite(value)
                    ? value
                    : EvaluateCondition.getToday(
                          property === "to" ? "end" : "begin"
                      )
            )
                .label(
                    supportsTime
                        ? pgettext("block:evaluate", "Use fixed date/time")
                        : pgettext("block:evaluate", "Use fixed date")
                )
                .features(
                    Forms.DateTimeFeatures.Date |
                        (supportsTime
                            ? Forms.DateTimeFeatures.TimeHoursAndMinutesOnly
                            : Forms.DateTimeFeatures.Weekday)
                )
                .years(
                    new Date().getFullYear() - 150,
                    new Date().getFullYear() + 50
                )
                .zone("UTC")
                .width("full")
                .required()
                .on((input) => {
                    if (input.isFormVisible && input.isObservable) {
                        this[property] = input.value;
                    }
                });

            const variableControl = new Forms.Dropdown(
                variables,
                isVar ? (value as string) : ""
            )
                .label(pgettext("block:evaluate", "Use value of"))
                .width("full")
                .on((variable) => {
                    if (
                        variable.isFormVisible &&
                        variable.isObservable &&
                        this.variable !== variable.value
                    ) {
                        this[property] = variable.value || "";
                    }
                });

            variableControls.push(variableControl);

            return this.editor
                .form({
                    title,
                    controls: [
                        new Forms.Radiobutton<"current" | "variable" | "date">(
                            [
                                {
                                    label: supportsTime
                                        ? pgettext(
                                              "block:evaluate",
                                              "Current date/time"
                                          )
                                        : pgettext(
                                              "block:evaluate",
                                              "Current date"
                                          ),
                                    value: "current",
                                },
                                {
                                    label: supportsTime
                                        ? pgettext(
                                              "block:evaluate",
                                              "Fixed date/time"
                                          )
                                        : pgettext(
                                              "block:evaluate",
                                              "Fixed date"
                                          ),
                                    value: "date",
                                },
                                {
                                    label: pgettext("block:evaluate", "Value"),
                                    value: "variable",
                                    disabled: variables.length === 0,
                                },
                            ],
                            isVar
                                ? "variable"
                                : isNumberFinite(value)
                                ? "date"
                                : "current"
                        ).on((type) => {
                            dateControl.visible(type.value === "date");
                            variableControl.visible(type.value === "variable");

                            if (
                                type.isFormVisible &&
                                type.isObservable &&
                                type.value === "current"
                            ) {
                                this[property] = undefined;
                            }
                        }),
                        dateControl,
                        variableControl,
                    ],
                })
                .visible(visible);
        };

        const fromDate = addCondition(
            "value",
            pgettext("block:evaluate", "If date equals"),
            false,
            this.isDate &&
                !this.supportsTime &&
                this.mode !== "defined" &&
                this.mode !== "undefined"
        );
        const fromDateTime = addCondition(
            "value",
            pgettext("block:evaluate", "If date/time equals"),
            true,
            this.isDate &&
                this.supportsTime &&
                this.mode !== "defined" &&
                this.mode !== "undefined"
        );
        const toDate = addCondition(
            "to",
            pgettext("block:evaluate", "And"),
            false,
            this.isDate &&
                !this.supportsTime &&
                (this.mode === "between" || this.mode === "not-between")
        );
        const toDateTime = addCondition(
            "to",
            pgettext("block:evaluate", "And"),
            true,
            this.isDate &&
                this.supportsTime &&
                (this.mode === "between" || this.mode === "not-between")
        );

        return {
            update: (
                oldVariable: string | undefined,
                newVariable: string | undefined
            ) => {
                form.visible(this.isDate);

                modeControl.refresh();

                if (oldVariable !== newVariable) {
                    if (oldVariable) {
                        each(variableControls, (variableControl) => {
                            variableControl.optionDisabled(oldVariable, false);
                        });
                    }

                    if (newVariable) {
                        each(variableControls, (variableControl) => {
                            variableControl.optionDisabled(newVariable, true);
                        });
                    }
                }
            },
        };
    }

    @editor
    defineEditor(): void {
        this.editor.form({
            title: pgettext("block:evaluate", "Input"),
            controls: [
                new Forms.Dropdown(
                    populateVariables(
                        this,
                        (slot) =>
                            slot instanceof Slots.String ||
                            slot instanceof Slots.Text ||
                            slot instanceof Slots.Number ||
                            slot instanceof Slots.Numeric ||
                            slot instanceof Slots.Boolean ||
                            slot instanceof Slots.Date,
                        this.variable
                    ),
                    this.variable
                )
                    .placeholder(
                        pgettext(
                            "block:evaluate",
                            "Select the input variable to use..."
                        )
                    )
                    .on((variable) => {
                        const oldVariable = this.variable;

                        this.variable = variable.value || undefined;
                        this.slot =
                            (variable.value &&
                                lookupVariable(this, variable.value)?.slot) ||
                            undefined;

                        stringControls.update(oldVariable, this.variable);
                        numberControls.update(oldVariable, this.variable);
                        booleanControls.update(oldVariable, this.variable);
                        dateControls.update(oldVariable, this.variable);

                        if (!this.variable) {
                            this.value = undefined;
                        }

                        if (!this.variable || this.isString || this.isBoolean) {
                            this.to = undefined;
                        }

                        if (
                            !this.variable ||
                            this.isNumber ||
                            this.isDate ||
                            this.isBoolean
                        ) {
                            this.ignoreCase = undefined;
                        }
                    })
                    .autoFocus(),
            ],
        });

        const stringControls = this.stringEditor();
        const numberControls = this.numberEditor();
        const booleanControls = this.booleanEditor();
        const dateControls = this.dateEditor();

        stringControls.update(undefined, this.variable);
        numberControls.update(undefined, this.variable);
        booleanControls.update(undefined, this.variable);
        dateControls.update(undefined, this.variable);
    }
}
