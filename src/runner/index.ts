/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    DateTime,
    Num,
    Slots,
    Str,
    castToBoolean,
    castToNumber,
    castToString,
    condition,
    isFilledString,
    isNumberFinite,
    isString,
    isVariable,
    tripetto,
} from "tripetto-runner-foundation";
import { TMode } from "./mode";

export interface IEvaluateCondition {
    readonly variable?: string;
    readonly mode?: TMode;
    readonly value?: string | number;
    readonly to?: string | number;
    readonly ignoreCase?: boolean;
}

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
})
export class EvaluateCondition extends ConditionBlock<IEvaluateCondition> {
    private getMatchString() {
        const value = castToString(this.props.value);

        if (isVariable(value)) {
            const variable = this.variableFor(value);

            return variable && variable.hasValue ? variable.string : "";
        }

        return this.parseVariables(value, "", true);
    }

    private getNumber(slot: Slots.Slot, value: number | string | undefined) {
        if (
            isString(value) &&
            (slot instanceof Slots.Number || slot instanceof Slots.Numeric)
        ) {
            const variable = this.variableFor(value);

            return variable && variable.hasValue
                ? slot.toValue(variable.value)
                : undefined;
        }

        return isNumberFinite(value) ? value : undefined;
    }

    private getDate(slot: Slots.Date, value: number | string | undefined) {
        if (isString(value) && slot instanceof Slots.Date) {
            const variable = this.variableFor(value);

            return variable && variable.hasValue
                ? slot.toValue(
                      variable.value,
                      (slot.supportsTime && "minutes") || undefined
                  )
                : undefined;
        }

        return slot.toValue(
            isNumberFinite(value) ? value : DateTime.UTC,
            (slot.supportsTime && "minutes") || undefined
        );
    }

    @condition
    validate(): boolean {
        if (this.props.variable) {
            const variable = this.variableFor(this.props.variable);

            if (variable) {
                if (
                    variable.slot instanceof Slots.String ||
                    variable.slot instanceof Slots.Text
                ) {
                    const match = this.props.ignoreCase
                        ? Str.lowercase(this.getMatchString())
                        : this.getMatchString();
                    const value = this.props.ignoreCase
                        ? Str.lowercase(variable.string)
                        : variable.string;

                    switch (this.props.mode) {
                        case "equal":
                            return value === match;
                        case "not-equal":
                            return value !== match;
                        case "contains":
                            return (
                                (match && value.indexOf(match) !== -1) || false
                            );
                        case "not-contains":
                            return (
                                (match && value.indexOf(match) === -1) || false
                            );
                        case "starts":
                            return (
                                (match && value.indexOf(match) === 0) || false
                            );
                        case "ends":
                            return (
                                (match &&
                                    value.lastIndexOf(match) ===
                                        value.length - match.length) ||
                                false
                            );
                        case "defined":
                            return value !== "";
                        case "undefined":
                            return value === "";
                    }
                } else if (
                    variable.slot instanceof Slots.Number ||
                    variable.slot instanceof Slots.Numeric
                ) {
                    const value = this.getNumber(
                        variable.slot,
                        this.props.value
                    );

                    switch (this.props.mode) {
                        case "equal":
                            return (
                                (variable.hasValue
                                    ? variable.value
                                    : undefined) === value
                            );
                        case "not-equal":
                            return (
                                (variable.hasValue
                                    ? variable.value
                                    : undefined) !== value
                            );
                        case "below":
                            return (
                                isNumberFinite(value) &&
                                variable.hasValue &&
                                castToNumber(variable.value) < value
                            );
                        case "above":
                            return (
                                isNumberFinite(value) &&
                                variable.hasValue &&
                                castToNumber(variable.value) > value
                            );
                        case "between":
                        case "not-between":
                            const to = this.getNumber(
                                variable.slot,
                                this.props.to
                            );

                            return (
                                isNumberFinite(value) &&
                                isNumberFinite(to) &&
                                (variable.hasValue &&
                                    castToNumber(variable.value) >=
                                        Num.min(value, to) &&
                                    castToNumber(variable.value) <=
                                        Num.max(value, to)) ===
                                    (this.props.mode === "between")
                            );
                        case "defined":
                            return variable.hasValue;
                        case "undefined":
                            return !variable.hasValue;
                    }
                } else if (variable.slot instanceof Slots.Date) {
                    const value = this.getDate(variable.slot, this.props.value);
                    const input = variable.hasValue
                        ? variable.slot.toValue(
                              variable.value,
                              (variable.slot.supportsTime && "minutes") ||
                                  undefined
                          )
                        : undefined;

                    switch (this.props.mode) {
                        case "equal":
                            return input === value;
                        case "not-equal":
                            return input !== value;
                        case "before":
                            return (
                                isNumberFinite(value) &&
                                isNumberFinite(input) &&
                                input < value
                            );
                        case "after":
                            return (
                                isNumberFinite(value) &&
                                isNumberFinite(input) &&
                                input > value
                            );
                        case "between":
                        case "not-between":
                            const to = this.getDate(
                                variable.slot,
                                this.props.to
                            );

                            return (
                                isNumberFinite(value) &&
                                isNumberFinite(to) &&
                                (isNumberFinite(input) &&
                                    input >= Num.min(value, to) &&
                                    input <= Num.max(value, to)) ===
                                    (this.props.mode === "between")
                            );
                        case "defined":
                            return variable.hasValue;
                        case "undefined":
                            return !variable.hasValue;
                    }
                } else if (variable.slot instanceof Slots.Boolean) {
                    switch (this.props.mode) {
                        case "true":
                            return (
                                variable.hasValue &&
                                castToBoolean(variable.value)
                            );
                        case "false":
                            return (
                                variable.hasValue &&
                                !castToBoolean(variable.value)
                            );
                        case "equal":
                        case "not-equal":
                            const value =
                                (isFilledString(this.props.value) &&
                                    this.variableFor(this.props.value)) ||
                                undefined;
                            return (
                                (value &&
                                    (variable.hasValue
                                        ? castToBoolean(variable.value)
                                        : undefined) ===
                                        (value.hasValue
                                            ? castToBoolean(value.value)
                                            : undefined)) ||
                                false
                            );
                        case "defined":
                            return variable.hasValue;
                        case "undefined":
                            return !variable.hasValue;
                    }
                }
            }
        }

        return false;
    }
}
